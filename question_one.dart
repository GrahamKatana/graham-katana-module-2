void main() {
  String name = "Graham Kenneth Katana";
  String favoriteApp = "Smartlearn";
  String city = "Johannesburg";
  String about = printMessage(name, favoriteApp, city);
  print(about);
}

String printMessage(String name, String app, String city) {
  return "My name is $name and my favorite app is $app. I am from the city of $city";
}
