import 'dart:convert';

void main() {
  var apps = winningApps();
  //------------------------------------------------------------------------
  //part a
  //------------------------------------------------------------------------
  sortNames(apps);

  //------------------------------------------------------------------------
  //part b
  //------------------------------------------------------------------------
  String winning2017 = winningAppsFilter(2017, apps);
  String winning2018 = winningAppsFilter(2018, apps);
  formatMessages("$winning2017, $winning2018 respectively",
      "Winning apps for 2017 and 2018", "");

  //------------------------------------------------------------------------
  //part c
  //------------------------------------------------------------------------
  int winningAppsAll = allApps(apps.length);
  formatMessages(winningAppsAll, "Winning apps from 2013 to 2021",
      "Total number of winning apps:");
}

void formatMessages(var arg, String heading, String message) {
  print("+-------------------------------------------------------");
  print("|$heading                                       ");
  print("+-------------------------------------------------------");
  print("|$message $arg\n");
}

List winningApps() {
  final jsonData =
      '{"apps":[{"name":"Ambani","year":2021,"developer":"Ambani Africa","sector":"Education"},{"name":"EasyEquities","year":2020,"developer":"easyequities.io","sector":"Finance"},{"name":"Naked Insurance","year":2019,"developer":"www.naked.insure","sector":"Finance"},{"name":"Khula","year":2018,"developer":"dev@khula.co.za","sector":"Business"},{"name":"Standard Bank Shyft","year":2017,"developer":"Standard Bank","sector":"Finance"},{"name":"Domestly","year":2016,"developer":"","sector":"Business"},{"name":"WumDrop","year":2015,"developer":"za.wumdrop.com","sector":"Maps and Navigation"},{"name":"Live Inspect","year":2014,"developer":"","sector":"Business"},{"name":"SnapScan","year":2013,"developer":"www.snapscan.co.za","sector":"Finance"},{"name":"FNB Banking","year":2012,"developer":"FNB","sector":"Finance"}]}';

  final parsedJson = json.decode(jsonData);
  var arr = parsedJson['apps'];
  return arr;
}

//-------------------------------------------------------------------------
//a
//-------------------------------------------------------------------------
void sortNames(List apps) {
  final List<String> sortedNames = <String>[];
  for (int i = 0; i < apps.length; i++) {
    sortedNames.add(apps[i]["name"]);
  }
  sortedNames.sort();
  print("----------------------------------------------------");
  print("Sorted App Names");
  print("----------------------------------------------------");
  for (int i = 0; i < sortedNames.length; i++) {
    print(sortedNames[i]);
    print("----------------------------------------------------");
  }
}

//-------------------------------------------------------------------------
//b
//-------------------------------------------------------------------------
String winningAppsFilter(int year, List apps) {
  String name = "None";
  for (int i = 0; i < apps.length; i++) {
    if (year == apps[i]["year"]) {
      name = apps[i]["name"];
    }
  }
  return name;
}

//-------------------------------------------------------------------------
//c
//-------------------------------------------------------------------------
int allApps(allApps) {
  return allApps;
}
