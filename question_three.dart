import 'dart:convert';

class Apps {
  Apps(
      {required this.name,
      required this.year,
      required this.developer,
      required this.sector});
  final String name;
  final String developer;
  final String sector;
  final int year;

  String toUpperCase(String name) {
    return name.toUpperCase();
  }
}

main() {
  final jsonData =
      '{"apps":[{"name":"Ambani","year":2021,"developer":"Ambani Africa","sector":"Education"},{"name":"EasyEquities","year":2020,"developer":"easyequities.io","sector":"Finance"},{"name":"Naked Insurance","year":2019,"developer":"www.naked.insure","sector":"Finance"},{"name":"Khula","year":2018,"developer":"dev@khula.co.za","sector":"Business"},{"name":"Standard Bank Shyft","year":2017,"developer":"Standard Bank","sector":"Finance"},{"name":"Domestly","year":2016,"developer":"","sector":"Business"},{"name":"WumDrop","year":2015,"developer":"za.wumdrop.com","sector":"Maps and Navigation"},{"name":"Live Inspect","year":2014,"developer":"","sector":"Business"},{"name":"SnapScan","year":2013,"developer":"www.snapscan.co.za","sector":"Finance"},{"name":"FNB Banking","year":2012,"developer":"FNB","sector":"Finance"}]}';

  final parsedJson = json.decode(jsonData);
  var arr = parsedJson['apps'];
  for (int i = 0; i < arr.length; i++) {
    var data = new Apps(
      name: arr[i]["name"],
      year: arr[i]["year"],
      developer: arr[i]["developer"],
      sector: arr[i]["sector"],
    );
    print("--------------------------------------------------------");
    print("|Name:" + data.toUpperCase(data.name));
    print("|Sector:" + data.sector);
    print("|Developer:" + data.developer);
    print("|Year:" + data.year.toString());
  }
  ;
}
